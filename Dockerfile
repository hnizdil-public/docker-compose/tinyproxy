FROM alpine:3.12

RUN apk add --no-cache tinyproxy

ARG PORT
ARG BASIC_AUTH_USER
ARG BASIC_AUTH_PASS

RUN sed -i \
	-e "s/^Allow /#Allow /" \
	-e "s/^Port .*/Port ${PORT}/" \
	-e "s/#BasicAuth user password/BasicAuth ${BASIC_AUTH_USER} ${BASIC_AUTH_PASS}/" \
	/etc/tinyproxy/tinyproxy.conf

EXPOSE ${PORT}

ENTRYPOINT ["/usr/bin/tinyproxy"]

CMD ["-d"]
